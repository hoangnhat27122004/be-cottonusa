import mongoose from "mongoose";
// const slug = require("mongoose-slug-generator");
import slug from "mongoose-slug-updater";
import mongoosePaginate from "mongoose-paginate-v2";
mongoose.plugin(slug);

const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    discount: {
      type: Number,
    },

    // chất liệu
    material: {
      type: String,
      required: true,
    },
    variants: [
      {
        color: {
          type: String,
          required: true,
        },
        thumbnail: [
          {
            imageUrl: {
              type: String,
              required: true,
            }
          },
        ],
        option: [
          {
            size: {
              type: String,
              required: true,
            },
            quantity: {
              type: Number,
              default: 0,
              min: 0,
            },
          },
        ],
      },
    ],
    sku: {
      type: String,
      required: true,
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category",
      required: true,
    },
    collections: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Collection",
      required: true,
    },
    slug: {
      type: String,
      slug: "name",
      unique: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);
productSchema.plugin(mongoosePaginate);
export default mongoose.model("Product", productSchema);
