import { Router } from "express";
import routerProduct from "./product.js";
import routerCategory from "./category.js";
import routerCollection from "./collection.js";
import routerAccount from "./account.js";
const router = Router();
router.use("/product", routerProduct);
router.use("/category", routerCategory);
router.use("/collection", routerCollection);
router.use("/account", routerAccount);

export default router;
