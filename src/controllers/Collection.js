import Category from "../models/Category.js";
import Collection from "../models/Collection.js";
import Product from "../models/Product.js";
import { collectionValidator } from "../validations/collection.js";

export const create = async (req, res) => {
  try {
    const { error } = collectionValidator.validate(req.body, {
      abortEarly: false,
    });
    if (error) {
      const errors = error.details.map((err) => err.message);
      return res.status(400).json({
        message: errors,
      });
    }
    const data = await Collection(req.body).save();
    if (!data) {
      throw new Error(`Error creating`);
    }
    const categories = req.body.category; // Assumption: categories là một mảng các ID danh mục
    const categoryUpdates = await Promise.all(
      categories.map(async (categoryId) => {
        return Category.findByIdAndUpdate(
          categoryId,
          {
            $push: { // dùng push để thêm 1 giá trị vào cuối mảng khi sử dụng push mongo sẽ thêm mà kh kiểm tra xem giá trị đó đã tồn tại hay chưa
              // addToSet chỉ thêm khi giá trị đó chưa tồn tại trong mảng
              collections: data._id,
            },
          },
          { new: true }
        );
      })
    );

    const failedUpdates = categoryUpdates.filter(update => !update);
    if (failedUpdates.length > 0) {
      throw new Error(`One or more categories failed to update`);
    }

    // if (!updateCategory) {
    //   throw new Error(`Category not found`);
    // }
    return res.status(200).json({
      data,
    });
  } catch (error) {
    return res.json({
      name: error.name,
      message: error.message,
    });
  }
};

export const get = async (req, res) => {
  try {
    const data = await Collection.find({}).populate(
      "products category",
      "name"
    );
    if (!data) {
      throw new Error(`Failed to get collections`);
    }
    return res.status(200).json({
      data,
    });
  } catch (error) {
    return res.json({
      name: error.name,
      message: error.message,
    });
  }
};
export const getById = async (req, res) => {
  try {
    const data = await Collection.findById(req.params.id);
    // .populate("products");
    if (!data) {
      throw new Error(`Failed to get collection detail`);
    }
    return res.status(200).json({
      data,
    });
  } catch (error) {
    return res.json({
      name: error.name,
      message: error.message,
    });
  }
};
export const update = async (req, res) => {
  try {
    const { error } = collectionValidator.validate(req.body, {
      abortEarly: false,
      allowUnknown: true,
    });
    if (error) {
      const errors = error.details.map((err) => err.message);
      return res.status(400).json({
        message: errors,
      });
    }
    const oldData = await Collection.findById(req.params.id);
    const removeCategory = await Category.findByIdAndUpdate(oldData.category, {
      $pull: {
        category: oldData._id,
      },
    });
    if (!removeCategory) {
      throw new Error(`Can't get category`);
    }
    const data = await Collection.findByIdAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true }
    );
    if (!data) {
      throw new Error(`Failed to update collection`);
    }
    const updateCategory = await Category.findByIdAndUpdate(data.category, {
      $addToSet: {
        category: data._id,
      },
    });
    if (!updateCategory) {
      throw new Error(`Category not found`);
    }
    return res.status(200).json({
      data,
    });
  } catch (error) {
    return res.json({
      name: error.name,
      message: error.message,
    });
  }
};

export const remove = async (req, res) => {
  try {
    const data = await Collection.findByIdAndDelete({ _id: req.params.id });
    await Product.findByIdAndUpdate(data.products, {
      $pull: {
        collections: data._id,
      },
    });
    await Category.findByIdAndUpdate(data.category, {
      $pull: {
        collections: data._id,
      },
    });
    if (!data) {
      throw new Error(`Failed to delete collection`);
    }
    return res.status(200).json({
      message: "Collection deleted successfully",
      data,
      a,
    });
  } catch (error) {
    return res.json({
      name: error.name,
      message: error.message,
    });
  }
};
